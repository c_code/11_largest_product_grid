#include <stdio.h>
#include <stdlib.h>

#define GRID_DIM 20
#define OPERANDS 4

/**@brief Taks in an array of pointers and finds the largest product in the diagonals going up and to the left
 *
 *@param int* pintGrid: Array of numbers to use to find find the largest product
 *@param int x: x position in the array
 *@param int y: y position in the array
 *@param int intLrgstPrdct: Current largest prodcut in the array
 *
 *@ret Returns the largest product found so far
 */
int checkDiagonalUpL(int* pintGrid, int x, int y, int intLrgstPrdct)
{
	int sum=1;
	int i;

	for(i = 0; i < OPERANDS; i++){
		if(y < OPERANDS || x < OPERANDS){
			break;
		}
		else{
			sum*=pintGrid[(y - i) * GRID_DIM + x - i];
		}
	}

	intLrgstPrdct = sum > intLrgstPrdct ? sum : intLrgstPrdct;
	
	return intLrgstPrdct;
}

/**@brief Taks in an array of pointers and finds the largest product in the diagonals going up and to the right
 *
 *@param int* pintGrid: Array of numbers to use to find find the largest product
 *@param int x: x position in the array
 *@param int y: y position in the array
 *@param int intLrgstPrdct: Current largest prodcut in the array
 *
 *@ret Returns the largest product found so far
 */
int checkDiagonalUpR(int* pintGrid, int x, int y, int intLrgstPrdct)
{
	int sum=1;
	int i;

	for(i = 0; i < OPERANDS; i++){
		if(y < OPERANDS || x > (GRID_DIM - OPERANDS)){
			break;
		}
		else{
			sum*=pintGrid[(y - i) * GRID_DIM + x + i];
		}
	}

	intLrgstPrdct = sum > intLrgstPrdct ? sum : intLrgstPrdct;
	
	return intLrgstPrdct;
}

/**@brief Taks in an array of pointers and finds the largest product in the columns of the array
 *
 *@param int* pintGrid: Array of numbers to use to find find the largest product
 *@param int x: x position in the array
 *@param int y: y position in the array
 *@param int intLrgstPrdct: Current largest prodcut in the array
 *
 *@ret Returns the largest product found so far
 */
int checkColumn(int* pintGrid, int x, int y, int intLrgstPrdct)
{
	int sum=1;
	int i;

	for(i = 0; i < OPERANDS; i++){
		if(y < OPERANDS){
			break;
		}
		else{
			sum*=pintGrid[(y - i) * GRID_DIM + x];
		}
	}

	intLrgstPrdct = sum > intLrgstPrdct ? sum : intLrgstPrdct;
	
	return intLrgstPrdct;
}

/**@brief Taks in an array of pointers and finds the largest product in the rows of the array
 *
 *@param int* pintGrid: Array of numbers to use to find find the largest product
 *@param int x: x position in the array
 *@param int y: y position in the array
 *@param int intLrgstPrdct: Current largest prodcut in the array
 *
 *@ret Returns the largest product that was found so far
 */
int checkRow(int* pintGrid, int x, int y, int intLrgstPrdct)
{
	int sum=1;
	int i;

	for(i = 0; i < OPERANDS; i++){
		if(x < OPERANDS){
			break;
		}
		else{
			sum*=pintGrid[y * GRID_DIM + x - i];
		}
	}

	intLrgstPrdct = sum > intLrgstPrdct ? sum : intLrgstPrdct;
	
	return intLrgstPrdct;
}

/**@brief Print the grid of a 20x20 array
 *
 *@param int *pintGrid: Array of numbers to be printed
 *
 *@ret Retruns 0 on success
 */
int printGrid(int *pintGrid){
	
	int x, y;

	for(y = 0; y < GRID_DIM; y++){
		for(x = 0; x < GRID_DIM; x++){
			printf("%2d,", pintGrid[y * GRID_DIM + x]);
		}
		printf("\n");
	}

	return 0;
}
/**@brief Read in a file of 20x20 integers
 *
 *Reads in a file that holds two digit integers organized in a 20x20 array and delimited by spaces
 *
 *@param int *pintGrid: Array to be filled with numbers from a file
 *
 *@ret Returns 0 on success, -1 of failure
 */
int readGrid(int *pintGrid)
{
	FILE* pfIn;
	int i = 0;
	int j = 0;
	char pchrTemp[8];

	/* open file */
	pfIn = fopen("grid.txt", "r");
	if(!pfIn){
		fprintf(stderr, "Could not open file: grid.txt\n");
		return -1;
	}

	/* read numbers in skipping spaces and newlines */
	while((pchrTemp[i] = fgetc(pfIn)) != EOF){
		if(pchrTemp[i] == ' ' || pchrTemp[i] == '\n'){
			pchrTemp[i] = '\0';
			pintGrid[j] = atoi(pchrTemp);
			j++;
			i = 0;
		}
		else{
			i++;
		}
	}
	
	if(fclose(pfIn)){ 
		fprintf(stderr, "Error closing file: grid.txt\n");
		return -1;
	}

	return 0;
}

/**@brief Finds the largest product in a grid by checking all possible numbers that connect in a line to the current position that is being checked in the grid
 *
 *@param int* pintGrid: Array of numbers to use to find find the largest product
 *@param int x: x position in the array
 *@param int y: y position in the array
 *@param int intLrgstPrdct: Current largest prodcut in the array
 *
 *@ret Returns the largest product found so far
 */
int findLargestProduct(int* pintGrid, int x, int y, int intLrgstPrdct)
{
	intLrgstPrdct = checkRow(pintGrid, x, y, intLrgstPrdct);
	intLrgstPrdct = checkColumn(pintGrid, x, y, intLrgstPrdct);
	intLrgstPrdct = checkDiagonalUpL(pintGrid, x, y, intLrgstPrdct);
	intLrgstPrdct = checkDiagonalUpR(pintGrid, x, y, intLrgstPrdct);		       

	return intLrgstPrdct;
}

/**@brief Find the greatest product in a 20x20 array consisting of four numbers
 *
 *ret Returns 0 on success -1 on failure
 */
int main(int argc, char *argv[])
{
	/* init */
	int pintGrid[400]; 
	int ret = 0;
	int intLrgstPrdct = 0;
	int x, y;

	ret = readGrid(pintGrid);
	ret = printGrid(pintGrid);
	
	for(y = 0; y < GRID_DIM; y++){
		for(x = 0; x < GRID_DIM; x++){
			intLrgstPrdct = findLargestProduct(pintGrid, x, y, intLrgstPrdct);
		}
	}

	printf("The largest product of the grid is %d\n", intLrgstPrdct);

	return ret;
}
